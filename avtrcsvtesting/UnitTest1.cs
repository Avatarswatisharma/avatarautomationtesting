﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Support.UI;
using System.Linq;
using System.IO;
using System.Threading;
using RelevantCodes.ExtentReports;
using NUnit.Framework.Interfaces;
using System.Collections;
using System.Drawing.Imaging;

namespace avtrcsvtesting
{
    [TestFixture]
    public class UnitTest1
    {
        public ExtentReports extent;
        public ExtentTest test;
        string  actualPath, projectPath, reportPath, csvPath = string.Empty;
        string path = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
        [OneTimeSetUp]
        public void StartReport()
        {
            actualPath = path.Substring(0, path.LastIndexOf("bin"));
            projectPath = new Uri(actualPath).LocalPath;
            reportPath = projectPath + "Reports\\MyOwnReport.html";
            if(File.Exists(reportPath))
            {
                File.Delete(reportPath);
            }
            reportPath = projectPath + "Reports\\MyOwnReport.html";
            extent = new ExtentReports(reportPath, true);

            extent.LoadConfig(projectPath + "XMLFile1.xml");

            test = extent.StartTest("aShowapprverReset");
            NUnit.Framework.Assert.IsTrue(true);
            test.Log(LogStatus.Pass, "Passed");
            extent.EndTest(test);

            test = extent.StartTest("bManagerApproveExtendRevoke");
            NUnit.Framework.Assert.IsTrue(true);
            test.Log(LogStatus.Pass, "Passed");
            extent.EndTest(test);

            test = extent.StartTest("cRequestCancel");
            NUnit.Framework.Assert.IsTrue(true);
            test.Log(LogStatus.Pass, "Passed");
            extent.EndTest(test);

            test = extent.StartTest("dRejectRequestbyManager");
            NUnit.Framework.Assert.IsTrue(true);
            test.Log(LogStatus.Pass, "Passed");
            extent.EndTest(test);

            test = extent.StartTest("eReasignRequestbyManager");
            NUnit.Framework.Assert.IsTrue(true);
            test.Log(LogStatus.Pass, "Passed");
            extent.EndTest(test);

            test = extent.StartTest("fExtendRevokeRequestbyManager");
            NUnit.Framework.Assert.IsTrue(true);
            test.Log(LogStatus.Pass, "Passed");
            extent.EndTest(test);
        }
        


        [TearDown]
        public void AfterClass()
        {
            //StackTrace details for failed Testcases
            var status = NUnit.Framework.TestContext.CurrentContext.Result.Outcome.Status;
            var stackTrace = " " + NUnit.Framework.TestContext.CurrentContext.Result.StackTrace + "";
            var errorMessage = NUnit.Framework.TestContext.CurrentContext.Result.Message;
            if (status == TestStatus.Failed)
            {
                test.Log(LogStatus.Fail, status + errorMessage);
            }
            

            //End test report

            // extent.EndTest(test);
            // driver.Quit();

        }

        IWebDriver driver = null;
        string serviceType = string.Empty;
        string service = string.Empty;
        List<string> inputList = new List<string>();
        IJavaScriptExecutor jsExecutor;
        IWebElement WebFindElement;

        //setup
        public UnitTest1()
        {
            actualPath = path.Substring(0, path.LastIndexOf("bin"));
            projectPath = new Uri(actualPath).LocalPath;
            string csvPath = projectPath + "inputFile.csv";

            //  var reader = new StreamReader(File.OpenRead(@"C:\Users\Sqtk-Swati\Desktop\inputFile.csv"));
            var reader = new StreamReader(File.OpenRead(csvPath));          

            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                inputList.Add(line);
            }

            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            login(inputList[1], inputList[2]);
        }

        [OneTimeTearDown]
        public void EndReport()
        {
            Thread.Sleep(2000);
            //driver.Close();
            extent.Flush();
            extent.Close();
        }
        /// <summary>
        /// Method for login in Avatar
        /// </summary>
        /// <param name="name"></param>
        /// <param name="password"></param>
        public void login(string name, string password)
        {
            driver.Navigate().GoToUrl(inputList[0]);
            driver.FindElement(By.ClassName("sq-login-username")).SendKeys(name);
            driver.FindElement(By.ClassName("sq-login-password")).SendKeys(password);

            WebFindElement = driver.FindElement(By.ClassName("sq-login-orange-btn"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
        }

        /// <summary>
        ///  Method for log out from Avatar
        /// </summary>
        public void logout()
        {
            WebFindElement = driver.FindElement(By.ClassName("userlgout"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
        }


        /// <summary>
        /// Method for selecting the date from date picker
        /// </summary>
        /// <param name="datename"></param>
        /// <param name="dateselected"></param>
        public void dateSetting(string datename, string dateselected)
        {
            string yearPath1 = string.Empty, yearPath2 = string.Empty, yearPath3 = string.Empty, monthPath = string.Empty;
            WebFindElement = driver.FindElement(By.Name(datename));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            DateTime date = Convert.ToDateTime(dateselected);

            string dy = date.Day.ToString();
            int mn = date.Month;
            string monthmn = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(mn);
            string yy = date.Year.ToString();

            if (datename == "StartDate")
            {
                yearPath1 = "/html/body/div[7]/div[5]/table/thead/tr/th[2]";
                yearPath2 = "/html/body/div[7]/div[3]/table/thead/tr[1]/th[2]";
                yearPath3 = "/html/body/div[7]/div[5]/table/tbody/tr/td/span";
                monthPath = "/html/body/div[7]/div[4]/table/tbody/tr/td/span";

            }
            else if (datename == "EndDate")
            {
                yearPath1 = "/html/body/div[8]/div[5]/table/thead/tr/th[2]";
                yearPath2 = "/html/body/div[8]/div[3]/table/thead/tr[1]/th[2]";
                yearPath3 = "/html/body/div[8]/div[5]/table/tbody/tr/td/span";
                monthPath = "/html/body/div[8]/div[4]/table/tbody/tr/td/span";
            }

            //clicking June 2018
            WebFindElement = driver.FindElement(By.XPath(yearPath1));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            //clicking 2018
            WebFindElement = driver.FindElement(By.XPath(yearPath2));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            //Searching of year 2018 from the list of years
            var year = driver.FindElements(By.XPath(yearPath3));
            foreach (var selectedYear in year)
            {

                if (selectedYear.Text == yy)
                {
                    selectedYear.Click();
                    break;
                }
            }

            //Searching of month from the list of months
            var month = driver.FindElements(By.XPath(monthPath));
            foreach (var selectedday in month)
            {

                if (selectedday.Text == monthmn)
                {
                    selectedday.Click();
                    break;
                }
            }

            //Searching of date from the list of months
            var day = driver.FindElements(By.ClassName("day"));
            foreach (var selectedday in day)
            {
                if (selectedday.Text == dy)
                {
                    selectedday.Click();
                    break;
                }
            }
        }


        void ManagerClick(string id, string message)
        {
            List<IWebElement> dashboardOption = driver.FindElements(By.ClassName("sqSwitchConsole")).ToList();
            foreach (IWebElement switchConsole in dashboardOption)
            {
                switchConsole.FindElement(By.ClassName("sqNavIcon")).Click();
                break;
            }

            driver.SwitchTo().Window(driver.WindowHandles[0]).Close(); // close the tab
           // driver.SwitchTo().Window(driver.WindowHandles[0]);

            //switch to last tab
            driver.SwitchTo().Window(driver.WindowHandles.Last());

            int count = 0;

            var table = driver.FindElement(By.TagName("table"));
            var rows = table.FindElements(By.TagName("tr"));
            foreach (var row in rows)
            {
                if (count == 1)
                {
                    row.Click();
                    break;
                }
                else
                {
                    count++;
                }
            }
            Thread.Sleep(10000);
            WebFindElement = driver.FindElement(By.Id("ReqEdit"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            Thread.Sleep(2000);
            //clicking approve confirmation button
            WebFindElement = driver.FindElement(By.Id(id));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);


            List<IWebElement> dynamicDivs1 = driver.FindElements(By.CssSelector("#bootdiv1 > div > div")).ToList();
            foreach (var item1 in dynamicDivs1)
            {
                List<IWebElement> dynamicDivs2 = item1.FindElements(By.CssSelector("#bootdiv1 > div > div > div.modal-body > div")).ToList();
                foreach (var item2 in dynamicDivs2)
                {
                    List<IWebElement> dynamicDivs3 = item2.FindElements(By.CssSelector("#InboxActionListForm")).ToList();
                    foreach (var item3 in dynamicDivs3)
                    {
                        Thread.Sleep(3000);
                        item3.FindElement(By.CssSelector("#avatarinboxcomments")).SendKeys(message);
                        break;
                    }
                }
            }

        }

        /// <summary>
        /// Show Approve button and RESET button click
        /// </summary>
       
        [Test, Order(1)]
        public void aShowapprverReset()
        {
            Thread.Sleep(2000);
            List<IWebElement> requestOption = driver.FindElements(By.XPath("//*[@id='NewReqids']")).ToList();
            foreach (IWebElement NewRequest in requestOption)
            {
                if (NewRequest.Text.Equals(inputList[3]))
                {
                    Thread.Sleep(1000);
                    NewRequest.Click();
                    break;
                }
            }

            //entering  USB Access in Search
            IWebElement searchField = driver.FindElement(By.Id("searchme"));
            searchField.SendKeys(inputList[4]);

            jsExecutor.ExecuteScript("arguments[0].click();", searchField);


            //clicking the usb(service type)
            serviceType = inputList[5];
            driver.FindElement(By.Name(serviceType)).Click();

            //clicking the service
            service = inputList[6];

            SelectElement oSelect = new SelectElement(driver.FindElement(By.Id("listOfEnt")));
            oSelect.SelectByText(service);


            WebDriverWait customWait = new WebDriverWait(driver, TimeSpan.FromMinutes(5));

            customWait = new WebDriverWait(driver, TimeSpan.FromMinutes(5));

            dateSetting("StartDate", inputList[14]);

            //clicking Appover button           
            new WebDriverWait(driver, TimeSpan.FromMinutes(5)).Until(ExpectedConditions.ElementIsVisible(By.ClassName("showapp-btn"))).Click();

            Thread.Sleep(5000);

            //close the approve popup
            new WebDriverWait(driver, TimeSpan.FromMinutes(5)).Until(ExpectedConditions.ElementIsVisible(By.ClassName("bootbox-close-button"))).Click();

            //write the Bussiness justification
            IWebElement justification = driver.FindElement(By.Name("justification"));
            justification.SendKeys(inputList[7]);

            //Clicking RESET
            WebFindElement = driver.FindElement(By.ClassName("reset-btn"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
            Thread.Sleep(5000);

            //Clicking Confirm
            WebFindElement = driver.FindElement(By.XPath("//button[.='Confirm']"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
            

        }

       
        /// <summary>
        /// SUBMIT Button Testing
        /// </summary>
        /// <param name="entitlement"></param>
        /// <param name="changeDate"></param>
        public void Submit(string entitlement ,bool changeDate)
        {
            if (driver.Url != inputList[24])
            {
                driver.Navigate().GoToUrl(inputList[24]);
            }

            //Search
            IWebElement searchField = driver.FindElement(By.Id("searchme"));
            searchField.Clear();
            searchField.SendKeys(inputList[8]);

            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", searchField);

            //clicking the service type
            serviceType = inputList[9];
            IWebElement wServiceType = driver.FindElement(By.Name(serviceType));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", wServiceType);

            if(changeDate == true)
            {
                dateSetting("StartDate", inputList[14]);
                dateSetting("EndDate", inputList[20]);
            }
            else
            {
                dateSetting("EndDate", inputList[23]);
            }
           

            //clicking the service
            SelectElement oSelect = new SelectElement(driver.FindElement(By.Id("listOfEnt")));
            oSelect.SelectByText(entitlement);

            //write the Bussiness justification
            IWebElement justification = driver.FindElement(By.Name("justification"));
            justification.SendKeys(entitlement);

            //Clicking SUBMIT            
            wServiceType = driver.FindElement(By.CssSelector("#btnsubmit:nth-child(2)"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", wServiceType);
            Thread.Sleep(5000);

            IList<IWebElement> links = driver.FindElements(By.TagName("a"));
            links.First(element1 => element1.Text == inputList[12]).Click();
            Thread.Sleep(5000);
        }


        /// <summary>
        ///  Approving of Request by Manager and extending the request by the user ,Approving by manager and Revoking of request by user
        ///  1. User Raising the request
        ///  2. manager approving the request
        ///  3. User extending the end date of request 
        ///  4. Manager Again approving the request
        ///  5. User Revoking the Request 
        /// </summary>

        [Test, Order(2)]
        public void bManagerApproveExtendRevoke()
        {
            Submit(inputList[11], false);
            //bSubmit(searchList[6]); -- TESTING
            logout();
            login(inputList[13], inputList[2]);

            Thread.Sleep(1000);
            ManagerClick("APPROVE","Request Approved as a part of automation ");

            Thread.Sleep(5000);
            //Clicking Confirm
            WebFindElement = driver.FindElement(By.XPath("//button[.=' APPROVE']"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
            Thread.Sleep(5000);

            logout();
            login(inputList[1], inputList[2]);
            Thread.Sleep(2000);
            List<IWebElement> requestOption = driver.FindElements(By.XPath("//*[@id='reqid']")).ToList();
            foreach (IWebElement NewRequest in requestOption)
            {
                if (NewRequest.Text.Equals(inputList[19]))
                {

                    NewRequest.Click();
                    break;
                }
            }

            Thread.Sleep(3000);
            WebFindElement = driver.FindElement(By.XPath("//*[@id='Datalist']/td[9]/span[1]/img"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            //dateSetting("EndDate", searchList[22]);
            Thread.Sleep(1000);
            WebFindElement = driver.FindElement(By.Name("EndDate"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            DateTime date = Convert.ToDateTime(inputList[22]);

            string dy = date.Day.ToString();
            int mn = date.Month;
            string monthmn = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(mn);
            string yy = date.Year.ToString();

            //clicking June 2018
            WebFindElement = driver.FindElement(By.XPath("/html/body/div[9]/div[3]/table/thead/tr[1]/th[2]"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            //clicking 2018
            WebFindElement = driver.FindElement(By.XPath("/html/body/div[9]/div[4]/table/thead/tr/th[2]"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            var year = driver.FindElements(By.XPath("/html/body/div[9]/div[5]/table/tbody/tr/td/span"));
            foreach (var selectedYear in year)
            {
                if (selectedYear.Text == yy)
                {
                    selectedYear.Click();
                    break;
                }
            }

            //clicking of month
            var month = driver.FindElements(By.XPath("/html/body/div[9]/div[4]/table/tbody/tr/td/span"));
            foreach (var selectedday in month)
            {

                if (selectedday.Text == monthmn)
                {
                    selectedday.Click();
                    break;
                }
            }

            //clicking of date
            var day = driver.FindElements(By.ClassName("day"));
            foreach (var selectedday in day)
            {
                if (selectedday.Text == dy)
                {
                    selectedday.Click();
                    break;
                }
            }
            Thread.Sleep(2000);

            WebFindElement = driver.FindElement(By.Id("btnUpdateServiceAction"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
            Thread.Sleep(2000);
            logout();
            login(inputList[13], inputList[2]);
           

            Thread.Sleep(1000);
            ManagerClick("APPROVE", "Request to extend Approved as a part of automation ");            
            Thread.Sleep(5000);
            //Clicking Confirm
            WebFindElement = driver.FindElement(By.XPath("//button[.=' APPROVE']"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
            Thread.Sleep(5000);
           // driver.Close();
            logout();
            login(inputList[1], inputList[2]);
            //logout();
            Thread.Sleep(2000);
            requestOption = driver.FindElements(By.XPath("//*[@id='reqid']")).ToList();
            foreach (IWebElement NewRequest in requestOption)
            {
                if (NewRequest.Text.Equals(inputList[19]))
                {

                    NewRequest.Click();
                    break;
                }
            }

            //Revoke
            Thread.Sleep(5000);
            WebFindElement = driver.FindElement(By.Id("imgid"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
            Thread.Sleep(2000);
            WebFindElement = driver.FindElement(By.Id("btnUpdateServiceAction"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            Thread.Sleep(2000);

        }
        /// <summary>
        /// Cancelling of Request by User
        /// </summary>
        [Test, Order(3)]
        public void cRequestCancel()
        {
            Thread.Sleep(2000);
            
            //login(inputList[1], inputList[2]);
            Submit(inputList[15],true);

            IList<IWebElement> links = driver.FindElements(By.TagName("a"));
            links.First(element1 => element1.Text == inputList[12]).Click();

            int count = 0;
            var table = driver.FindElement(By.Id("myrequestlayout"));
            var tr = table.FindElements(By.TagName("tr"));
            foreach (var row in tr)
            {
                var td = table.FindElements(By.TagName("td"));
                foreach (var td1 in td)
                {
                    if (count == 6)
                    {
                        Thread.Sleep(3000);
                        td1.Click();
                        Thread.Sleep(3000);
                        //Clicking Confirm
                        IWebElement element11 = driver.FindElement(By.XPath("//button[.='Confirm']"));
                        jsExecutor = (IJavaScriptExecutor)driver;
                        jsExecutor.ExecuteScript("arguments[0].click();", element11);
                        Thread.Sleep(3000);
                        count++;
                        break;
                    }
                    else
                    {
                        count++;
                    }

                }
            }
        }

        /// <summary>
        /// Rejecting the request by manager which is raised the user
        /// 1. User raising the request 
        /// 2. Manager rejecting the request
        /// </summary>
        [Test, Order(4)]
        public void dRejectRequestbyManager()
        {
            Submit(inputList[16],true);
            logout();
            login(inputList[13], inputList[2]);

            Thread.Sleep(2000);

            ManagerClick("REJECT", "Request Rejected as a part of automation ");
            
            Thread.Sleep(3000);
            //Clicking Reject
            WebFindElement = driver.FindElement(By.XPath("//button[.=' REJECT']"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            Thread.Sleep(3000);
            logout();
            login(inputList[1], inputList[2]);

        }

        /// <summary>
        /// 1. User Raising a request 
        /// 2. Approver reassigning the request to another manager(demo0)
        /// 3. demo0 approving the request
        /// 4. Manager extending and revoking the service
        /// </summary>

        [Test, Order(5)]
        public void eReasignRequestbyManager()
        {
            Thread.Sleep(2000);
            Submit(inputList[17], false);
            Thread.Sleep(1000);
            logout();
            login(inputList[13], inputList[2]);

            Thread.Sleep(2000);           

            List<IWebElement> dashboardOption = driver.FindElements(By.ClassName("sqSwitchConsole")).ToList();
            foreach (IWebElement switchConsole in dashboardOption)
            {
                switchConsole.FindElement(By.ClassName("sqNavIcon")).Click();
                break;
            }

            Thread.Sleep(2000);
            driver.SwitchTo().Window(driver.WindowHandles[0]).Close(); // close the tab
            //switch to last tab
            driver.SwitchTo().Window(driver.WindowHandles.Last());

            int count = 0;

            var table = driver.FindElement(By.TagName("table"));
            var rows = table.FindElements(By.TagName("tr"));
            foreach (var row in rows)
            {
                if (count == 1)
                {
                    row.Click();
                    break;
                }
                else
                {
                    count++;
                }
            }
            Thread.Sleep(3000);
            WebFindElement = driver.FindElement(By.Id("ReqEdit"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            //clicking reassign
            WebFindElement = driver.FindElement(By.Id("REASSIGN"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
            Thread.Sleep(3000);

            List<IWebElement> redynamicDivs1 = driver.FindElements(By.CssSelector("#bootdiv1 > div > div")).ToList();
            foreach (var item1 in redynamicDivs1)
            {
                List<IWebElement> redynamicDivs2 = item1.FindElements(By.CssSelector("#bootdiv1 > div > div > div.modal-body > div")).ToList();
                foreach (var item2 in redynamicDivs2)
                {

                    List<IWebElement> redynamicDivs3 = item2.FindElements(By.CssSelector("#InboxActionListForm")).ToList();
                    foreach (var item3 in redynamicDivs3)
                    {
                        Thread.Sleep(3000);
                        item3.FindElement(By.CssSelector("#AvatarReassignTo")).SendKeys(inputList[27]);                        
                        Thread.Sleep(1000);
                        List<IWebElement> uls = driver.FindElements(By.XPath("//*[@id='ui-id-1']")).ToList();
                        foreach (var item in uls)
                        {
                            List<IWebElement> lis = item.FindElements(By.XPath("//li")).ToList();
                            foreach (var itemee in lis)
                            {
                               // if (itemee.Text == "DEMO0-stdtest demo0-stdtest.demo0@dpmdemo.com")
                                    if (itemee.Text == inputList[26] )
                                    {
                                    itemee.Click();
                                    break;
                                }
                            }

                            //}
                        }
                        Thread.Sleep(3000);
                      //  Thread.Sleep(3000);
                        break;
                    }
                }
            }


            List<IWebElement> dynamicDivs1 = driver.FindElements(By.CssSelector("#bootdiv1 > div > div")).ToList();
            foreach (var item1 in redynamicDivs1)
            {
                List<IWebElement> dynamicDivs2 = item1.FindElements(By.CssSelector("#bootdiv1 > div > div > div.modal-body > div")).ToList();
                foreach (var item2 in dynamicDivs2)
                {
                    List<IWebElement> dynamicDivs3 = item2.FindElements(By.CssSelector("#InboxActionListForm")).ToList();
                    foreach (var item3 in dynamicDivs3)
                    {
                        Thread.Sleep(3000);
                        item3.FindElement(By.CssSelector("#avatarinboxcomments")).SendKeys("Request Reassign as a part of automation");
                        break;
                    }
                }
            }

            Thread.Sleep(3000);

            //Clicking Reassign
            WebFindElement = driver.FindElement(By.XPath("//button[.=' REASSIGN']"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
            Thread.Sleep(3000);
          //  driver.Close();
            logout();
            login(inputList[21], inputList[2]);
            Thread.Sleep(2000);

            ManagerClick("APPROVE", "Request Approved as a part of automation ");
            
            //Clicking Confirm
            WebFindElement = driver.FindElement(By.XPath("//button[.=' APPROVE']"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            Thread.Sleep(3000);
           // driver.Close();
            logout();

            login(inputList[1], inputList[2]);
            Thread.Sleep(2000);
            List<IWebElement> requestOption = driver.FindElements(By.XPath("//*[@id='reqid']")).ToList();
            foreach (IWebElement NewRequest in requestOption)
            {
                if (NewRequest.Text.Equals(inputList[19]))
                {
                    Thread.Sleep(1000);
                    NewRequest.Click();
                    break;
                }
            }
            Thread.Sleep(5000);
        }


        [Test, Order(6)]
        public void fExtendRevokeRequestbyManager()
        {
            Thread.Sleep(2000);
            Submit(inputList[18], false);
            Thread.Sleep(1000);

            logout();
            login(inputList[13], inputList[2]);
            Thread.Sleep(2000);

            ManagerClick("APPROVE", "Request Approved as a part of automation ");

            //Clicking Confirm
            WebFindElement = driver.FindElement(By.XPath("//button[.=' APPROVE']"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

            Thread.Sleep(3000);

            logout();

            login(inputList[1], inputList[2]);
            Thread.Sleep(2000);
            List<IWebElement> requestOption = driver.FindElements(By.XPath("//*[@id='reqid']")).ToList();
            foreach (IWebElement NewRequest in requestOption)
            {
                if (NewRequest.Text.Equals(inputList[19]))
                {
                    Thread.Sleep(1000);
                    NewRequest.Click();
                    break;
                }
            }
            Thread.Sleep(5000);

            logout();

            login(inputList[21], inputList[2]);
            Thread.Sleep(2000);
            List<IWebElement> dashboardOption = driver.FindElements(By.ClassName("sqSwitchConsole")).ToList();
            foreach (IWebElement switchConsole in dashboardOption)
            {
                switchConsole.FindElement(By.ClassName("sqNavIcon")).Click();
                break;
            }

            Thread.Sleep(2000);
            driver.SwitchTo().Window(driver.WindowHandles[0]).Close(); // close the tab
            //switch to last tab
            driver.SwitchTo().Window(driver.WindowHandles.Last());

            requestOption = driver.FindElements(By.XPath("//*[@id='reqid']")).ToList();
            foreach (IWebElement NewRequest in requestOption)
            {
                if (NewRequest.Text.Equals(inputList[25]))
                {
                    Thread.Sleep(1000);
                    NewRequest.Click();
                    break;
                }
            }

            //Clicking Confirm
            WebFindElement = driver.FindElement(By.ClassName("own-user"));
            jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
            int count = 0;
            bool finished = false;
            var table = driver.FindElement(By.Id("myreqtable"));
            var rows = table.FindElements(By.TagName("tr"));
            Thread.Sleep(2000);
            foreach (var row in rows)
            {
                if (count == 2)
                {
                    row.Click();
                    var subTable = driver.FindElements(By.ClassName("childtr"));

                    foreach (var subrow in subTable)
                    {
                        var subtd = driver.FindElements(By.Name("imgrevoke"));
                        foreach (var item in subtd)
                        {
                            jsExecutor = (IJavaScriptExecutor)driver;
                            jsExecutor.ExecuteScript("arguments[0].click();", item);

                            Thread.Sleep(2000);
                            WebFindElement = driver.FindElement(By.Name("EndDate"));
                            jsExecutor = (IJavaScriptExecutor)driver;
                            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

                            DateTime date = Convert.ToDateTime(inputList[22]);

                            string dy = date.Day.ToString();
                            int mn = date.Month;
                            string monthmn = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(mn);
                            string yy = date.Year.ToString();

                            //clicking June 2018
                            WebFindElement = driver.FindElement(By.XPath("/html/body/div[9]/div[3]/table/thead/tr[1]/th[2]"));
                            jsExecutor = (IJavaScriptExecutor)driver;
                            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

                            //clicking 2018
                            WebFindElement = driver.FindElement(By.XPath("/html/body/div[9]/div[4]/table/thead/tr/th[2]"));
                            jsExecutor = (IJavaScriptExecutor)driver;
                            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

                            var year = driver.FindElements(By.XPath("/html/body/div[9]/div[5]/table/tbody/tr/td/span"));
                            foreach (var selectedYear in year)
                            {
                                if (selectedYear.Text == yy)
                                {
                                    selectedYear.Click();
                                    break;
                                }
                            }

                            //clicking of month
                            var month = driver.FindElements(By.XPath("/html/body/div[9]/div[4]/table/tbody/tr/td/span"));
                            foreach (var selectedday in month)
                            {

                                if (selectedday.Text == monthmn)
                                {
                                    selectedday.Click();
                                    break;
                                }
                            }

                            //clicking of date
                            var day = driver.FindElements(By.ClassName("day"));
                            foreach (var selectedday in day)
                            {
                                if (selectedday.Text == dy)
                                {
                                    selectedday.Click();
                                    break;
                                }
                            }
                            Thread.Sleep(2000);

                            WebFindElement = driver.FindElement(By.Id("btnUpdateServiceAction"));
                            jsExecutor = (IJavaScriptExecutor)driver;
                            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

                            Thread.Sleep(2000);

                            logout();
                            login(inputList[13], inputList[2]);
                            Thread.Sleep(2000);

                            ManagerClick("APPROVE", "Request Approved as a part of automation ");

                            //Clicking Confirm
                            WebFindElement = driver.FindElement(By.XPath("//button[.=' APPROVE']"));
                            jsExecutor = (IJavaScriptExecutor)driver;
                            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);

                            Thread.Sleep(3000);

                            logout();
                            login(inputList[21], inputList[2]);
                            Thread.Sleep(2000);
                            dashboardOption = driver.FindElements(By.ClassName("sqSwitchConsole")).ToList();
                            foreach (IWebElement switchConsole in dashboardOption)
                            {
                                switchConsole.FindElement(By.ClassName("sqNavIcon")).Click();
                                break;
                            }

                            Thread.Sleep(2000);
                            driver.SwitchTo().Window(driver.WindowHandles[0]).Close(); // close the tab
                            //switch to last tab
                            driver.SwitchTo().Window(driver.WindowHandles.Last());

                            requestOption = driver.FindElements(By.XPath("//*[@id='reqid']")).ToList();
                            foreach (IWebElement NewRequest in requestOption)
                            {
                                if (NewRequest.Text.Equals(inputList[25]))
                                {
                                    Thread.Sleep(1000);
                                    NewRequest.Click();
                                    break;
                                }
                            }

                            //Clicking Confirm
                            WebFindElement = driver.FindElement(By.ClassName("own-user"));
                            jsExecutor = (IJavaScriptExecutor)driver;
                            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
                             count = 0;
                            finished = false;
                            table = driver.FindElement(By.Id("myreqtable"));
                            rows = table.FindElements(By.TagName("tr"));
                            Thread.Sleep(2000);
                            foreach (var row1 in rows)
                            {
                                if (count == 2)
                                {
                                    row1.Click();
                                    var subTable1 = driver.FindElements(By.ClassName("childtr"));

                                    foreach (var subrow1 in subTable1)
                                    {
                                        var subtd1 = driver.FindElements(By.Name("imgrevoke"));
                                        foreach (var item1 in subtd)
                                        {
                                            //Revoke
                                            Thread.Sleep(5000);
                                            WebFindElement = driver.FindElement(By.Id("imgid"));
                                            jsExecutor = (IJavaScriptExecutor)driver;
                                            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
                                            Thread.Sleep(2000);
                                            WebFindElement = driver.FindElement(By.Id("btnUpdateServiceAction"));
                                            jsExecutor = (IJavaScriptExecutor)driver;
                                            jsExecutor.ExecuteScript("arguments[0].click();", WebFindElement);
                                            Thread.Sleep(2000);
                                            finished = true;
                                            break;
                                        }
                                        break;
                                    }
                                }
                                else
                                {
                                    count++;
                                }
                                if (finished == true)
                                {
                                    break;
                                }
                            }
                            break;
                        }
                        break;
                    }                    
                }
                else
                {
                    count++;
                }
                if(finished ==  true)
                {
                    break;
                }
               
            }
            logout();

            login(inputList[1], inputList[2]);
            Thread.Sleep(2000);
            IList<IWebElement> links = driver.FindElements(By.TagName("a"));
            links.First(element1 => element1.Text == inputList[12]).Click();
            Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot.SaveAsFile(projectPath + "file.png");
        }        
    }
}
